﻿using System;

namespace XamExercise
{
	public static class ConstantClass
	{
		internal static string strAppName="Account";
		internal static int intNofRows=5;
		internal static string strAlertMsg="Are you sure you want to SignOut?";
		internal static string strAlertSignOut="SignOut";
		internal static string strAlertCancel="Cancel";
	}
}

