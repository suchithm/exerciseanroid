﻿using System;

namespace XamExercise
{
	public class AccountClass
	{
		public string strName { get; set; }

		public string strEmailIdLabelText { get; set; } 
		public string strEmailId { get; set; } 

		public string strAddressLabelText { get; set; } 
		public string strAddress { get; set; } 

		public string strTextPreLink { get; set; } 
		public string strLink { get; set; } 
		public string strPostLink { get; set; } 

		public string strNavigationText { get; set; } 


	}
}

