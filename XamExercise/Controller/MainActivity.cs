﻿using System; 
using Android.App;
using Android.Content; 
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace XamExercise
{

	[Activity ( Label = "Account" , MainLauncher = true , Icon = "@drawable/icon", ScreenOrientation=Android.Content.PM.ScreenOrientation.Portrait )]
	public class MainActivity : Activity
	{ 
		ListView lstViewAccountDetails;
		List<AccountClass> lstAccountDetails;
		AccountAdapterClass objAccountAdapter;
		protected override void OnCreate ( Bundle bundle )
		{
			base.OnCreate ( bundle );  
			Window.RequestFeature (WindowFeatures.NoTitle); //disabled default title
			SetContentView ( Resource.Layout.Main ); 
			FnInitializeView ();
			FnBindListView ();

		}

		#region "Defined function"
		void FnInitializeView()
		{
			lstViewAccountDetails = FindViewById<ListView> ( Resource.Id.listViewAccountDetails ); 
			lstAccountDetails = new List<AccountClass> (); //to fill and pass the data to adapter
		}
		void FnBindListView()
		{
			FnGetAccountData ();
			if (objAccountAdapter != null) {
				objAccountAdapter.ActionSignOutSelected -= FnSignOutSelected;
				objAccountAdapter = null;
			}

			objAccountAdapter = new AccountAdapterClass (this, lstAccountDetails);
			objAccountAdapter.ActionSignOutSelected += FnSignOutSelected;  //action event from adapter class to notify the signout selection
			lstViewAccountDetails.Adapter = objAccountAdapter;
		}
		void FnSignOutSelected(string strSignOutText)
		{
			FnAlertMsg (GetString (Resource.String.app_name),ConstantClass.strAlertMsg,ConstantClass.strAlertCancel,ConstantClass.strAlertSignOut , this);
			Console.WriteLine (strSignOutText);
		} 

		void FnGetAccountData()
		{ 
			// here Considered random datasource for listview 
			var objAccont = new AccountClass ();

			objAccont.strName = "Suchith";

			objAccont.strEmailIdLabelText="Email Id";
			objAccont.strEmailId = "suchimadavu@gmail.com";

			objAccont.strAddressLabelText = "Address";
			objAccont.strAddress = "123sast \n Suite5NW \n Chicago 56060";

			objAccont.strTextPreLink="To update your user profile Please visit ";
			objAccont.strLink = "http://www.abcd.com";
			objAccont.strPostLink = " and goto my profile";

			objAccont.strNavigationText="SignOut";
			lstAccountDetails.Add (objAccont);
		}
		void FnAlertMsg (string strTitle,string strMsg,string strCancel,string strSignOut,Context context)
		{
			AlertDialog alertMsg = new AlertDialog.Builder (context).Create ();
			alertMsg.SetCancelable (false);
			alertMsg.SetTitle (strTitle);
			alertMsg.SetMessage (strMsg);
			alertMsg.SetButton2 (strCancel, delegate (object sender, DialogClickEventArgs e) {
				if (e.Which == -2) {
					alertMsg.Dismiss ();
					alertMsg = null;
				}
			});
			alertMsg.SetButton (strSignOut, delegate (object sender, DialogClickEventArgs e) {
				if (e.Which == -1) {
					Console.WriteLine ("Signed Out from app");
					alertMsg.Dismiss ();
					alertMsg = null;
				}
			});
			alertMsg.Show ();
		}
		#endregion
	}
}


