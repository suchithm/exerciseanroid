﻿using System;
using Android.App;
using System.Collections.Generic;
using Android.Widget;
using Android.Views;



namespace XamExercise
{
	public class AccountAdapterClass:BaseAdapter
	{
		Activity context;  
		List<AccountClass> lstAccountDetails;
		internal event Action<string> ActionSignOutSelected; 
		public AccountAdapterClass (Activity c,List<AccountClass> lstAccountDetailsArg  )
		{ 
			context = c;  
			lstAccountDetails = lstAccountDetailsArg;
		}
		public override int Count {
			get { return ConstantClass.intNofRows; }
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return 0;
		}
		// create a new ImageView for each item referenced by the Adapter
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			AccountViewHolderCalss vh = null;
			View view = convertView;
			//to recyclerow
              // recycle option is removed as dealing with the unique fixed rows
			vh = new AccountViewHolderCalss ();  

			switch (position) {
			case 0: //name
				view = context.LayoutInflater.Inflate (Resource.Layout.AccountNameCustomLayout, parent, false);  
				vh.txtName = view.FindViewById<TextView> (Resource.Id.txtName);
				vh.txtName.Text = lstAccountDetails [0].strName;
				view.Tag = vh;   
				break;
			case 1: //email
				view = context.LayoutInflater.Inflate (Resource.Layout.AccountEmailCustomLayout, parent, false);  
				vh.txtEmailAddressText = view.FindViewById<TextView> (Resource.Id.txtEmailIdLabelText);
				vh.txtEmailAddress = view.FindViewById<TextView> (Resource.Id.txtEmailId);

				vh.txtEmailAddressText.Text = lstAccountDetails [0].strEmailIdLabelText;
				vh.txtEmailAddress.Text = lstAccountDetails [0].strEmailId;
				view.Tag = vh;   
				break;

			case 2: //address
				view = context.LayoutInflater.Inflate (Resource.Layout.AccountAddressCustomLayout, parent, false);  
				vh.txtAddressLabel = view.FindViewById<TextView> (Resource.Id.txtAddressLabelText);
				vh.txtAddress = view.FindViewById<TextView> (Resource.Id.txtAddress);

				vh.txtAddressLabel.Text = lstAccountDetails [0].strAddressLabelText;
				vh.txtAddress.Text = lstAccountDetails [0].strAddress;
				view.Tag = vh;   
				break;

			case 3: //Link
				view = context.LayoutInflater.Inflate (Resource.Layout.AccountLinkCustomLayout, parent, false);  
				vh.txtPreLinkText = view.FindViewById<TextView> (Resource.Id.txtPreLink);
				vh.txtLink = view.FindViewById<TextView> (Resource.Id.txtLink);
				vh.txtPostLinkText = view.FindViewById<TextView> (Resource.Id.txtPostLink);

				vh.txtPreLinkText.Text = lstAccountDetails [0].strTextPreLink;
				vh.txtLink.Text = lstAccountDetails [0].strLink;
				vh.txtPostLinkText.Text = lstAccountDetails [0].strPostLink;
				view.Tag = vh;   
				break;

			case 4: //nav
				view = context.LayoutInflater.Inflate (Resource.Layout.AccountNavCustomLayout, parent, false);  
				vh.txtNavBarText = view.FindViewById<TextView> (Resource.Id.txtNavigationalItem);
				vh.imgNavIcon = view.FindViewById<ImageView> (Resource.Id.imgIconArrow);

				vh.txtNavBarText.Text = lstAccountDetails [0].strNavigationText;
				vh.InitializeView (view); //to add selected event
				view.Tag = vh;   
				break;  
			}

			vh.NavigationSelected = () => {
				
				if (ActionSignOutSelected != null)
					ActionSignOutSelected (lstAccountDetails [0].strNavigationText); 
				
			};  
 
			return view;
		}

	} 

	public class AccountViewHolderCalss : Java.Lang.Object
	{
		public Action NavigationSelected{get;set;}

		internal TextView txtName;

		internal TextView txtEmailAddressText;  
		internal TextView txtEmailAddress; 

		internal TextView txtAddressLabel; 
		internal TextView txtAddress;

		internal TextView txtPreLinkText; 
		internal TextView txtLink;
		internal TextView txtPostLinkText;

		internal TextView txtNavBarText;
		internal ImageView imgNavIcon; 

		public void InitializeView(View viewProduct)
		{ 
			viewProduct.Click += delegate(object sender, EventArgs e) {
				NavigationSelected();
			};
		}
	}
}

